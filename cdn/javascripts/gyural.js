/* Version 0.1 */

var gyural = {
    _eventLog: [],
    eventCounter: 0,
    instanceID: parseInt(Math.random()*100000),
    bakeInstance: function() {
        
        $('<div></div>')
            .prop('id', 'gyural_' + this.instanceID)
            .appendTo($('body'));
        
        this.instanceDomId = 'gyural_' + this.instanceID;
        return true;
        
    },
    instance: function() {
        return $('#' + this.instanceDomId);
    },
    events: {
        'api': function(relationship, response, obj) {
            
            var url = obj.attr('href') + '?callback=1';
            
            $.post(url, function(res) {
                relationship._sendEvent(response.id, res, obj);
            }, 'json');
            
        }
    },
    _sendEvent: function(eventID, res, obj) {
        
        var info = this._eventLog[eventID];
        this._eventLog[eventID].result = res;
        
        // Check if is allowed function execution
        if(info.callback == 1) {
            if(res.callback != undefined) {
                eval(res.callback);
            }
        }
        
        if(info.rebind == 1) {
            this.bindEvents();
        }
        
        this.instance().trigger('eventReady-' + eventID);
        
    },
    bindEvents: function() {
        
        var relationship = this;
        for(var key in this.events) {
            
            var selector = '[data-gyural=' + key + ']';
            
            $(selector).each(function() {
                
                if($(this).data('gyuralid') == undefined) {
                
                    relationship.eventCounter++;
                    relationship._eventLog[relationship.eventCounter] = {
                        id: relationship.eventCounter,
                        selector: '[data-gyural=' + key + ']',
                        key: key,
                        rebind: $(this).data('gyuralrebind') != undefined ? $(this).data('gyuralrebind') : 0,
                        callback: $(this).data('gyuralcallback') != undefined ? $(this).data('gyuralcallback') : 1,
                        event: $(this).data('gyuralevent') != undefined ? $(this).data('gyuralevent') : 'click',
                        waiting: $(this).data('gyuralwaiting') != undefined ? $(this).data('gyuralwaiting') : false
                    };
                    
                    $(this).data('gyuralid', relationship.eventCounter);
                    
                    var info = relationship._eventLog[relationship.eventCounter];
                    
                    $(this).on(info.event, function(e) {
                        
                        e.preventDefault();
                        var info = relationship._eventLog[$(this).data('gyuralid')];
                        
                        if(info.waiting != false) {
                            $(this).html(info.waiting);
                        }
                        
                        relationship.events[$(this).data('gyural')](relationship, info, $(this));
                       
                    });
                
                }
                
            });
            
        }
    },
    init: function() {
        if(this.bakeInstance()) {
            
            this.bindEvents();
            
        }
    }
};