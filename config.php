<?php

/*

----------
Gyural 1.8
----------

Configuration file

*/

$versioning = json_decode(implode(file('sys'.DIRECTORY_SEPARATOR.'version.json')));
$version = $versioning->working;

foreach($version as $key => $value) define($key, $value);

if($version->online == 0) {
	include 'sys'.DIRECTORY_SEPARATOR.'error-over.txt';
	die();
}

!defined('gyu') ? die('Error Loading configuration file.') : '';

/*

----------------------
Self Defined Constants
----------------------

*/

define('absolute', $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR);
define('application', absolute . 'app' . DIRECTORY_SEPARATOR);
define('libs', absolute . 'libs' . DIRECTORY_SEPARATOR);
define('langs', absolute . 'langs' . DIRECTORY_SEPARATOR);
define('upload', absolute . $version->uploadPath);
define('cache', absolute . cdn . 'cache' . DIRECTORY_SEPARATOR);

if(!defined('sendmail')) define('sendmail' , 'php');

if(!defined('uriLogin')) define('uriLogin' , $version->uri . $version->uriLogin);
if(!defined('uriUpload')) define('uriUpload' , uri . DIRECTORY_SEPARATOR . $version->uploadPath);

if(!defined('varDivider')) define('varDivider', ':'); // (override the ?key=vars always. If you want to use the standard syntax ?var=1&var2=2 etc.. replace the first ? with &)

setlocale(LC_TIME, 'it_IT');

/*

--------------
Regex Patterns
--------------

*/

$dbLink_pattern = '/(.+):\/\/(\S+):(\S+)@(\S+)\/(\S+)/i';

/*

--------------
Database Specs
--------------

*/

define('dbLink', $version->mysql);
define('excludeFieldPrefix', "gyu_");	// -> Set this CONSTANT to strip vars that starts with it string from being MYSQLERIALIZED
define('preventDuplicate', true);		// -> Set this to TRUE or FALSE to prevent duplicate in the CreateQuery('I', …); and ->hangExecute();

?>