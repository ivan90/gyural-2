<?php

### GYURAL ###

/*

----------
Gyural 1.8
----------

Filename: /app/api/_/api.ctrl.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 17/09/13
	
*/

class apiCtrl extends standardController {
	
	var $index_tollerant = true;
	var $format;
	
	function __construct() {
		
		$GLOBALS["api"] = true;

		if(!isset($_REQUEST["format"]))
			$_REQUEST["format"] = 'json';
		
		$this->format = $_REQUEST["format"];
		
	}
	
	function CtrlIndex() {
		
		$ok = false;
		
		$portions = explode('/', $_REQUEST["x"]);
		$guess = count($portions) - 1;
		
		if($guess == 0) {
			LoadApp('api',1)->NoInput();
			exit;
		}
		
		if(strstr($portions[$guess], '.')) {
			$available[] = $portions[$guess];
			$ok = true;
		}
		
		if(!$ok) {
			foreach($portions as $portion) {
				if(strstr($portion, '.'))
					$available[] = $portion;
			}
		}
		
		foreach($available as $shall) {
			
			$class = explode('.', $shall);
			$method = $class[count($class)-1];
			unset($class[count($class)-1]);
			
			$controller = implode('/', $class);
			
			if(isController($controller . '/' . $method))
				break;
			
		}
		
		$controller = LoadApp($controller, 1);
		$res = $controller->{'Api' . $method}();

		if(!$res) {
			$res["error"] = 1;
		}

		if($this->format == 'json') {
			header('Content-type: application/json');
			echo json_encode($res);
		}

		else if($this->format == 'php') {
			header('Content-type: text/plain');
			echo serialize($res);
		}
		
		else if($this->format == 'xml') {
			// Working on it.
		}
		
	}
	
	function CtrlHelp() {
		echo '<pre>';
		Application('/api/_v/help');
		
		echo "\n\n<hr />\n" . '<em>Gyural '.version.'</em>';
		echo '</pre>';
	}
	
	function NoInput() {
		echo 'Sorry, no request.';
	}

}

?>