<?php

isGyu();
MethodStandard();

$GLOBALS["api"] = 1;

$cache = true; ## SET TO FALSE, IF YOU DON'T WANT TO CACHE IT. (DISABLE IT IF YOU WANT REBUILD IT) ##

if($_GET["type"] == 'css' || $_GET["type"] == 'less') {
	$_GET["type"] = 'less';
	header('Content-type: text/css');
	$lookingDirectory = 'css';
	$lookingExt = 'less';
} else if($_GET["type"] == 'js') {
	header('Content-type: text/javascript');
	$lookingDirectory = 'javascripts';
	$lookingExt = 'js';
}

$files = $_SESSION["optimization"][$_GET["file"]];

$CachedFile = absolute . cdn . 'cache/' . md5($_GET["file"]);

foreach($files as $file) {

	if($file[0] == '*')
			$more_room = '../../' . str_replace(absolute, '', application) . '';
		else
			$more_room = '';
	
	$previsionFile = str_replace(array('//', '///', '///'), '/', absolute . cdn . $lookingDirectory . '/' . $more_room . str_replace('*', '', $file) . '.' . $lookingExt);
	
	if($lookingExt == 'less')
		if(!is_file($previsionFile) && strstr($previsionFile, '.css'))
			$previsionFile = str_replace('.less', '', $previsionFile);
	
	$real_files[] = $previsionFile;
	$filemtime[] = filemtime($previsionFile);
	
}

if(is_file($CachedFile) && $cache == true && filemtime($CachedFile) > max($filemtime)) {
	$output = file_get_contents($CachedFile);
}
else {
	foreach($real_files as $previsionFile) {
		if(is_file($previsionFile)) {
			$toParse = file_get_contents($previsionFile);
			if($_GET["type"] == 'less') {
				$less = LoadClass('lessc', 1);
				$tpm = $less->compile($toParse);
			} else if($_GET["type"] == 'scss') {
				
			} else if($_GET["type"] == 'js') {
				$tpm = $toParse . ';';
				$tpm = CallFunction('gyu_optimization', 'stripComments', $tpm);
			}
			
			$output[] = $tpm;
		} else {
			if($_GET["type"] == 'js')
				$output[] = 'console.log("gyu_optimization: no input file (...'.substr($previsionFile, -35).')");';
		}
	}
	$output = implode($output);
}

if(extension_loaded('zlib')) ob_start('ob_gzhandler');

echo $output;

if(extension_loaded('zlib')) ob_end_flush();

file_put_contents($CachedFile, $output);