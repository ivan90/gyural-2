<?php
set_time_limit(0);
isGyu();
MethodStandard();

CallFunction('gyu_bucket', 'rlogged');

Application('gyu_bucket/_commons/header');

$preset = json_decode(file_get_contents(application . 'gyu_bucket/backup/_presets/' . $_GET["preset"]));

$backupPID = date('d-m-y-H-i') . '_' . str_replace('.json', '', $_GET["preset"]);

$destination = application . 'gyu_bucket/backup/_log/' . $backupPID;
mkdir($destination);

echo $backupPID;

echo '<pre>';
echo '<strong>Starting backup</strong>' . "\n\n";
echo date('r') . "\n\n";
foreach($preset as $file) {
	
	if($file == '@database') {

		file_put_contents($destination . '/' . 'dump.sql', CallFunction('gyu_bucket', 'backupDB'));

	}

	else {

		echo 'Processing: <em>' . $file . '</em>' . "\n";
		
		if(is_dir(absolute . str_replace('/', '', $file))) {
			CallFunction('gyu_bucket','Zip', absolute . str_replace('/', '', $file), $destination . '/' . str_replace('/', '', $file) . '.zip');
		} else {
			copy(absolute . $file, $destination . '/' . $file);
		}
		echo 'Stored.' . "\n* * *\n";
		flush();
		ob_flush();

	} 
	
}

CallFunction('gyu_bucket', 'Zip', $destination, application . 'gyu_bucket/backup/_log/_zipped/' . $backupPID . '.zip', true);

if(is_file(application . 'gyu_bucket/backup/_log/_zipped/' . $backupPID . '.zip')) {
	rrmdir($destination);
}

echo date('r') . "\n\n";

echo '</pre>';
echo '<a href="/gyu_bucket/backup">All Done.</a>';