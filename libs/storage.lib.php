<?php

### GYURAL ###

/*

--------------------
Storage / Gyural 1.9
--------------------

Filename: /libs/storage.lib.php
 Version: 1.9
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 10/07/2014

TTL: 1 week

*/

class storage {
	
	function __construct() {
	}

	function get($key, $format = 'php') {

		$file = cache . 'storage/' . urldecode($key);

		if(is_file($file)) {
			$content = file_get_contents($file);
		} else
			return false;

		if(strlen($content) > 0) {
			$obj = unserialize($content);
			if(time() >= $obj->end_life) {
				$this->clear($key);
				return false;
			} else {
				$out = $obj->obj;
			}
		}

		if(isset($out)) {
			if($format == 'php')
				return $out;
			else if($format == 'json')
				return json_encode($out);
		}

	}

	function set($key, $value, $ttl = 604800) {
		echo 'a';
		$dir = cache . 'storage/';
		if(!is_dir($dir))
			if(!mkdir($dir))
				return false;

		$file = $dir . urlencode($key);

		$obj = new stdClass;
		$obj->obj = $value;
		$obj->ttl = $ttl;
		$obj->end_life = time() + $ttl;

		if(file_put_contents($file, serialize($obj))) {
			return true;
		} else
			return false;

	}

	function clear($key) {

		$file = cache . 'storage/' . urldecode($key);

		if(is_file($file))
			return unlink($file);

	}

}

?>