<?php

/*

----------
Gyural 1.8
----------

Filename: /funcs/autoload/methods.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 08/11/2013

-------
Methods
-------

*/

function IsApplicationCallable($page) {
	
	$r = null;
	if(is_dir(application . $page))
		$r = $page . "/index.php";
	else
		if(is_file(application . $page . ".php"))
			$r = $page . ".php";
	
	return $r;
	
}

function ApplicationDetail($application) {
	
	$p = application . $application . '/_/version.gapp';
	
	if(is_file($p)) {
		$info = explode("\n", file_get_contents($p));
		foreach($info as $a) {
			list($k, $v) = explode(':', $a);
			$return[trim($k)] = trim($v);
		}
		return $return;
	} else
		return '[NotReposited]';
	
}

// Methods for access to gyu
function isApplication($appName) { # 1.1 #
	if(is_dir(application . $appName))
		return $appName;
	else
		return false;
}

function MethodAjax() {
	if(!isAjax())
		die('This should be AJAX!!');
}

function isAjax() {
	if(@empty($_SERVER["HTTP_X_REQUESTED_WITH"]) && @strtolower($_SERVER["HTTP_X_REQUESTED_WITH"]) != "xmlhttprequest")
		return false;
	else
		return true;
}

function MethodStandard() {
	return true;
}

function MethodBufferSave($buffer) {

	file_put_contents($GLOBALS["gyu_buffer"][0], $buffer);
	return($buffer);

}

function MethodBuffer($timeToLive = 31556926) {

	if(dev) return false;

	$identifier = debug_backtrace();

	if(!isset($identifier[1]["class"]))
		$id = $identifier[0]["file"];
	else
		$id = $identifier[1]["class"] . '-' . $identifier[1]["function"];	

	$uniqueId = md5($id);

	$file = cache . 'sys/buffer/' . $uniqueId;
	
	if(is_file($file)) {
		$diffTime = time() - filemtime($file);

		if($diffTime > $timeToLive) {
			unlink($file);
		} else {
			readfile($file);
			die();
		}

	} else {
		$GLOBALS["gyu_buffer"] = array($file);
		ob_start("MethodBufferSave");
	}

}

/*

----------------
-- never used --
----------------

function MethodCron($frequency) {
#	MethodApplication();
	echo $frequency;
	die();
}
*/

function MethodDetect($type = null) {
	
	$info = $_SERVER["REQUEST_METHOD"];
	
	$methods = array(	'get' => false,
						'post' => false,
						'put' => false,
						'ssl' => false,
						'delete' => false,
						'head' => false,
						'ajax' => false,
						'app' => false,
						'api' => false
					);
	
	if($info == 'GET')
		$methods["get"] = true;
	else if($info == 'POST')
		$methods["post"] = true;
	
	if(isSsl())
		$methods["ssl"] = true;
	
	if(isAjax())
		$methods["ajax"] = true;
	
	if(isset($GLOBALS["app"]) && $GLOBALS["app"] == true)
		$methods["app"] = true;
	
	if(isset($GLOBALS["api"]) && $GLOBALS["api"] == true)
		$methods["api"] = true;

	if(!$type)
		return $methods;
	else
		return $methods[$type];
	
}

function HttpResponse($code) {
	if ($code !== NULL) {
		switch ($code) {
			case 100: $text = 'Continue'; break;
			case 101: $text = 'Switching Protocols'; break;
			case 200: $text = 'OK'; break;
			case 201: $text = 'Created'; break;
			case 202: $text = 'Accepted'; break;
			case 203: $text = 'Non-Authoritative Information'; break;
			case 204: $text = 'No Content'; break;
			case 205: $text = 'Reset Content'; break;
			case 206: $text = 'Partial Content'; break;
			case 300: $text = 'Multiple Choices'; break;
			case 301: $text = 'Moved Permanently'; break;
			case 302: $text = 'Moved Temporarily'; break;
			case 303: $text = 'See Other'; break;
			case 304: $text = 'Not Modified'; break;
			case 305: $text = 'Use Proxy'; break;
			case 400: $text = 'Bad Request'; break;
			case 401: $text = 'Unauthorized'; break;
			case 402: $text = 'Payment Required'; break;
			case 403: $text = 'Forbidden'; break;
			case 404: $text = 'Not Found'; break;
			case 405: $text = 'Method Not Allowed'; break;
			case 406: $text = 'Not Acceptable'; break;
			case 407: $text = 'Proxy Authentication Required'; break;
			case 408: $text = 'Request Time-out'; break;
			case 409: $text = 'Conflict'; break;
			case 410: $text = 'Gone'; break;
			case 411: $text = 'Length Required'; break;
			case 412: $text = 'Precondition Failed'; break;
			case 413: $text = 'Request Entity Too Large'; break;
			case 414: $text = 'Request-URI Too Large'; break;
			case 415: $text = 'Unsupported Media Type'; break;
			case 500: $text = 'Internal Server Error'; break;
			case 501: $text = 'Not Implemented'; break;
			case 502: $text = 'Bad Gateway'; break;
			case 503: $text = 'Service Unavailable'; break;
			case 504: $text = 'Gateway Time-out'; break;
			case 505: $text = 'HTTP Version not supported'; break;
			default:
				exit('Unknown http status code "' . htmlentities($code) . '"');
				break;
			}

			$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
			header($protocol . ' ' . $code . ' ' . $text);
			$GLOBALS['http_response_code'] = $code;
	} else {
		$code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);
	}
	return $code;
}

function MethodApplication() {
	$info = debug_backtrace();
	$thisApp = $info[0]["file"];
	if(empty($GLOBALS["validApplication"][$thisApp]))
		die('You can\'t call it directly');
}

## calling functions
function Application($app, $hideOtherApplication = null, $app_data = null) {
	
	if($hideOtherApplication == null) {
		if(isset($GLOBALS["hideOtherApplication"]) && $GLOBALS["hideOtherApplication"] == 1)
			return true;
	}
	
	$GLOBALS["hideOtherApplication"] = $hideOtherApplication;
	
	$GLOBALS["applicationGyural"][$app] = $_GET["x"];
	$info = Route($app, 1);
	
	$info->calling = 1;
	$info->app_data = $app_data;
	$info->exec();

	$applicationId = uniqid();
	$GLOBALS["validApplication"][application . $app] = $applicationId;
	if(is_file(application . $app))
		include application . $app;

}

# Check if SSL is enabled.
function isSsl() {
	if(!isset($_SERVER["HTTPS"]))
		return false;
	else if($_SERVER["HTTPS"] == 'on')
		return true;
	else
		return false;
}

# Check if the user is from a mobile device.
function isMobile() {
	$is_mobile = '0';
	if(preg_match('/(android|up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
		$is_mobile = 1;
	if((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE']))))
		$is_mobile = 1;
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
	$mobile_agents = array('w3c','acs-','alav','alca','amoi','andr','audi','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','oper','palm','pana','pant','phil','play','port','prox','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp','wapr','webc','winw','winw','xda','xda-');
	
	if(in_array($mobile_ua, $mobile_agents))
		$is_mobile = 1;
	
	if (isset($_SERVER['ALL_HTTP']))
		if (strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini') > 0)
			$is_mobile = 1;
	
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows') > 0)
		$is_mobile = 0;
	
	return $is_mobile;
}

if (!function_exists('getallheaders')) { 
	
	function getallheaders() { 
		$headers = ''; 
		foreach ($_SERVER as $name => $value) { 
			if (substr($name, 0, 5) == 'HTTP_') { 
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
			}
		} 
		return $headers; 
	}
	
} 

?>